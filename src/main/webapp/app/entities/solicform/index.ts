export * from './solicform.model';
export * from './solicform-popup.service';
export * from './solicform.service';
export * from './solicform-dialog.component';
export * from './solicform-delete-dialog.component';
export * from './solicform-detail.component';
export * from './solicform.component';
export * from './solicform.route';

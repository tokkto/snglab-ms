export class Constants {

    // Formulario 1 - Estado de Resultados
    // -----------------------------------------------------------------
    // INGRESOS DE VENTAS NETOS U OPERACIÓN
    public readonly FORM1_TOTAL_DE_INGRESOS = 'TOTAL DE INGRESOS';

    // COSTOS DE VENTAS U OPERACIÓN
    //  I. EMPRESAS DE PRODUCCIÓN (1)
    public readonly FORM1_MATERIA_PRIMA = 'Materia Prima (2)';
    public readonly FORM1_MANO_OBRA_DIRECTA = 'Mano de Obra Directa';
    public readonly FORM1_GASTOS_FABRICACION = 'Gastos de Fabricación (2)';
    public readonly FORM1_SUB_TOTAL = 'Sub-Total';
    public readonly FORM1_DIFERENCIA_INVENTARIO_PRODUCTO_TERMINADO = '(+/-) Diferencia Inventario Producto Terminado';
    public readonly FORM1_DIFERENCIA_INVENTARIO_PRODUCTO_PROCESO = '(+/-) Diferencia Inventario Producto en Proceso';
    public readonly FORM1_SUBTOTAL_PRODUCCION = 'SUBTOTAL';

    //  II. EMPRESAS DE SERVICIOS O COMERCIALIZADORA (1)
    public readonly FORM1_SUBTOTAL_SERVICIOS = 'SUBTOTAL';
    public readonly FORM1_TOTAL_COSTOS_TOTALES_VENTAS_OPERACIONES_SERVICIOS = 'TOTAL COSTOS TOTALES DE VENTAS, OPERACIÓN Y/O SERVICIOS';
    public readonly FORM1_UTILIDAD_BRUTA = 'UTILIDAD (PÉRDIDA) BRUTA';

    // GASTOS OPERATIVOS
    public readonly FORM1_GASTOS_VENTAS_DISTRIBUCION = 'Gastos de Ventas y Distribución (3)';
    public readonly FORM1_GASTOS_ADMINISTRACION = 'Gastos de Administración (3)';
    public readonly FORM1_TOTAL_GASTOS_OPERATIVOS = 'TOTAL DE GASTOS OPERATIVOS';
    public readonly FORM1_UTILIDAD_OPERATIVA = 'UTILIDAD (PÉRDIDA) OPERATIVA';
    public readonly FORM1_INGRESOS_FINANCIEROS = 'Ingresos Financieros (3)';
    public readonly FORM1_GASTOS_FINANCIEROS = 'Gastos Financieros (3)';
    public readonly FORM1_ENAJENACION_VALORES_BIENES_ACTIVO_FIJO = 'Enajenación de Valores y Bienes del Activo Fijo';
    public readonly FORM1_COSTO_ENAJENACION_VALORES_BIENES_ACTIVO_FIJO = 'Costo de Enajenación de Valores y Bienes del Activo Fijo';
    public readonly FORM1_OTROS_INGRESOS = 'Otros Ingresos (3)';
    public readonly FORM1_OTROS_EGRESOS = 'Otros Egresos (3)';
    public readonly FORM1_SUBTOTAL_GASTOS_OPERATIVOS = 'SUBTOTAL';

    public readonly FORM1_UTILIDAD_ANTES_PARTICIPACIONES = 'UTILIDAD (PÉRDIDA) ANTES DE PARTICIPACIONES	';
    public readonly FORM1_PARTICIPACION_TRABAJADORES = 'Participación de los Trabajadores';
    public readonly FORM1_UTILIDAD_ANTES_IMPUESTOS = 'UTILIDAD (PÉRDIDA) ANTES DEL IMPUESTO';
    public readonly FORM1_IMPUESTO_RENTA = 'Impuesto a la Renta';
    public readonly FORM1_UTILIDAD_EJERCICIO = 'UTILIDAD (PÉRDIDA) DEL EJERCICIO';

    // Codigos
    // INGRESOS DE VENTAS NETOS U OPERACIÓN
    public readonly FORM1_COD_TOTAL_DE_INGRESOS = 'f1_tingreso';

    // COSTOS DE VENTAS U OPERACIÓN
    //  I. EMPRESAS DE PRODUCCIÓN (1)
    public readonly FORM1_COD_MATERIA_PRIMA = 'f1_mprima';
    public readonly FORM1_COD_MANO_OBRA_DIRECTA = 'f1_modirecta';
    public readonly FORM1_COD_GASTOS_FABRICACION = 'f1_gasfrab';
    public readonly FORM1_COD_SUB_TOTAL = 'f1_subtotal';
    public readonly FORM1_COD_DIFERENCIA_INVENTARIO_PRODUCTO_TERMINADO = 'f1_difinvproterm';
    public readonly FORM1_COD_DIFERENCIA_INVENTARIO_PRODUCTO_PROCESO = 'f1_difinvpropro';
    public readonly FORM1_COD_SUBTOTAL_PRODUCCION = 'f1_subtotalprod';

    //  II. EMPRESAS DE SERVICIOS O COMERCIALIZADORA (1)
    public readonly FORM1_COD_SUBTOTAL_SERVICIOS = 'f1_subtotalserv';
    public readonly FORM1_COD_TOTAL_COSTOS_TOTALES_VENTAS_OPERACIONES_SERVICIOS = 'f1_tcostvopser';
    public readonly FORM1_COD_UTILIDAD_BRUTA = 'f1_utibruta';

    // GASTOS OPERATIVOS
    public readonly FORM1_COD_GASTOS_VENTAS_DISTRIBUCION = 'f1_gasvendis';
    public readonly FORM1_COD_GASTOS_ADMINISTRACION = 'f1_gasadmin';
    public readonly FORM1_COD_TOTAL_GASTOS_OPERATIVOS = 'f1_tgasope';
    public readonly FORM1_COD_UTILIDAD_OPERATIVA = 'f1_utioper';
    public readonly FORM1_COD_INGRESOS_FINANCIEROS = 'f1_ingfinan';
    public readonly FORM1_COD_GASTOS_FINANCIEROS = 'f1_gasfinan';
    public readonly FORM1_COD_ENAJENACION_VALORES_BIENES_ACTIVO_FIJO = 'f1_envabiacfi';
    public readonly FORM1_COD_COSTO_ENAJENACION_VALORES_BIENES_ACTIVO_FIJO = 'f1_coenvabiacfi';
    public readonly FORM1_COD_OTROS_INGRESOS = 'f1_otroing';
    public readonly FORM1_COD_OTROS_EGRESOS = 'f1_otroegr';
    public readonly FORM1_COD_SUBTOTAL_GASTOS_OPERATIVOS = 'f1_subtotalgasop';

    public readonly FORM1_COD_UTILIDAD_ANTES_PARTICIPACIONES = 'f1_utiantparti';
    public readonly FORM1_COD_PARTICIPACION_TRABAJADORES = 'f1_partitrab';
    public readonly FORM1_COD_UTILIDAD_ANTES_IMPUESTOS = 'f1_utiantimp';
    public readonly FORM1_COD_IMPUESTO_RENTA = 'f1_imprenta';
    public readonly FORM1_COD_UTILIDAD_EJERCICIO = 'f1_utiejerc';
    // -----------------------------------------------------------------
}

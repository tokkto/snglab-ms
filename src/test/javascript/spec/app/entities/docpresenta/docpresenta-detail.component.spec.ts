/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DocpresentaDetailComponent } from '../../../../../../main/webapp/app/entities/docpresenta/docpresenta-detail.component';
import { DocpresentaService } from '../../../../../../main/webapp/app/entities/docpresenta/docpresenta.service';
import { Docpresenta } from '../../../../../../main/webapp/app/entities/docpresenta/docpresenta.model';

describe('Component Tests', () => {

    describe('Docpresenta Management Detail Component', () => {
        let comp: DocpresentaDetailComponent;
        let fixture: ComponentFixture<DocpresentaDetailComponent>;
        let service: DocpresentaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [DocpresentaDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DocpresentaService,
                    JhiEventManager
                ]
            }).overrideTemplate(DocpresentaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DocpresentaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DocpresentaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Docpresenta(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.docpresenta).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
